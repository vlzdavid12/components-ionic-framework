export interface DataJson {
  icon: string;
  name: string;
  redirecTo: string;
}

export interface  AlbumJson {
  userId: number;
  id: number;
  title: string;
}

export interface HeroesJson {
  superhero: string;
  publisher: string;
  // eslint-disable-next-line @typescript-eslint/naming-convention
  alter_ego: string;
  // eslint-disable-next-line @typescript-eslint/naming-convention
  first_appearance: string;
  characters: string;
}
