import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AlbumJson, DataJson, HeroesJson} from '../interface/interface';
import {delay} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DataService {


  constructor(private http: HttpClient) { }


  getUsers(){
    return this.http.get('https://jsonplaceholder.typicode.com/users');
  }

  getMenuOptions(){
    return this.http.get<DataJson[]>('/assets/data/menu-options.json');
  }

  getAlbums(){
    return this.http.get<AlbumJson[]>('https://jsonplaceholder.typicode.com/albums');
  }

  getHeroes(){
    return this.http.get<HeroesJson[]>('/assets/data/segments-options.json')
      .pipe(delay(2000));
  }


}
