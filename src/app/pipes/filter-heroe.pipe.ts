import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'filterHeroe'
})
export class FilterHeroePipe implements PipeTransform {

  transform(array: any[],
            tap: string = '',
            column: string = 'title'): any[] {

    if (tap.length <= 0) {
      return null;
    }

    if (!array) {
      return array;
    }

    tap = tap.toLocaleLowerCase();

    if (tap === 'todos') {
      return array;
    }
    return array.filter(item => item[column].toLowerCase().includes(tap));

  }

}
