import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(array: any[], txt: string = '', column: string = 'title'): any[] {

    // console.log(array);
    // console.log(txt);

    if (txt.length <= 0) {
      return array;
    }

    if (!array) {
      return array;
    }

    txt = txt.toLowerCase();
    return array.filter(item => item[column].toLowerCase().includes(txt));
  }

}
