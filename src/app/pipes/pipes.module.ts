import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FilterPipe} from './filter.pipe';
import { FilterHeroePipe } from './filter-heroe.pipe';


@NgModule({
  declarations: [
    FilterPipe,
    FilterHeroePipe
  ],
    exports: [
        FilterPipe,
        FilterHeroePipe
    ],
  imports: [
    CommonModule
  ],
})
export class PipesModule {
}
