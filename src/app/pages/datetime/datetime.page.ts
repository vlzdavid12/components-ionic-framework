import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-datetime',
  templateUrl: './datetime.page.html',
  styleUrls: ['./datetime.page.scss'],
})
export class DatetimePage implements OnInit {

  dateNac: Date = new Date();
  customYearValues: number[] = [2020, 2016, 2008, 2004, 2000, 1996];


  customPickerOptions = {
    buttons: [{
      text: 'Hola',
      handler: (event) => console.log(event)
    },
      {
        text: 'Mudo',
        handler: () => console.log('Hola mundo...')
      }
    ]
  };

  constructor() {
  }

  ngOnInit() {
  }

  changeDate(event) {
    console.log(event);
    console.log(new Date(event.detail.value))
  }




}
