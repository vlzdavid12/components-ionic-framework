import {Component, OnInit} from '@angular/core';
import {MenuController} from '@ionic/angular';
import {Observable} from 'rxjs';
import {DataJson} from '../../interface/interface';
import {DataService} from '../../service/data.service';

interface Components {
  icon: string;
  name: string;
  redirectTo: string;
}

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.page.html',
  styleUrls: ['./inicio.page.scss'],
})
export class InicioPage implements OnInit {

  components: Observable<DataJson[]>;

  constructor(private menu: MenuController,
              private dataService: DataService) {
  }

  ngOnInit() {
    this.components = this.dataService.getMenuOptions();

  }

  showMenu(){
    this.menu.open('first');
  }

}
