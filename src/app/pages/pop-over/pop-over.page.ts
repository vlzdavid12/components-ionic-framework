import { Component, OnInit } from '@angular/core';
import {PopoverController} from '@ionic/angular';
import {PopOverInfoComponent} from '../../components/pop-over-info/pop-over-info.component';

@Component({
  selector: 'app-pop-over',
  templateUrl: './pop-over.page.html',
  styleUrls: ['./pop-over.page.scss'],
})
export class PopOverPage implements OnInit {


  constructor(public popoverController: PopoverController) { }

  ngOnInit() {
  }

  onClick(){

  }

  async presentPopover(ev: any) {
    const popover = await this.popoverController.create({
      component: PopOverInfoComponent,
      event: ev,
      cssClass: 'my-custom-class',
      backdropDismiss: false,
      translucent: true
    });
    await popover.present();

    const {data:{item}} = await popover.onWillDismiss();
    console.log('Data modal: ', item);
  }

}
