import {Component, OnInit} from '@angular/core';
import {DataService} from '../../service/data.service';
import {Observable} from 'rxjs';
import {HeroesJson} from '../../interface/interface';

@Component({
  selector: 'app-segment',
  templateUrl: './segment.page.html',
  styleUrls: ['./segment.page.scss'],
})
export class SegmentPage implements OnInit {

  dataHeroe: Observable<HeroesJson[]>;

  publisher: string = 'todos';

  menuData: {
    value: string;
    name: string;
  }[] = [
    {
      value: 'todos',
      name: 'Todos',
    },
    {
      value: 'DC Comics',
      name: 'DC Comics',
    }, {
      value: 'Marvel Comics',
      name: 'Marvel Comics',
    }
  ];

  constructor(private dataService: DataService) {
  }

  ngOnInit() {
    this.dataHeroe = this.dataService.getHeroes();
  }


  segmentChanged(event) {
    const {detail: {value}} = event;
    this.publisher = value;
  }

}
