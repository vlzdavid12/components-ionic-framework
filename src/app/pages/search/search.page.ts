import {Component, OnInit} from '@angular/core';
import {DataService} from '../../service/data.service';
import {AlbumJson} from '../../interface/interface';
import {Observable} from 'rxjs';
import {tap, map} from 'rxjs/operators';

@Component({
  selector: 'app-search',
  templateUrl: './search.page.html',
  styleUrls: ['./search.page.scss'],
})
export class SearchPage implements OnInit {

  textSearch: string =  '';
  albums: AlbumJson[] = [];

  constructor(private dataService: DataService) {
  }

  ngOnInit() {
    this.dataService.getAlbums().subscribe(albums => {
      this.albums = albums;
    });
  }

  onSearch(event: any) {
    const {target: {value}} = event;
    this.textSearch =  value;
  }

}
