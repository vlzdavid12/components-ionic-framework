import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FloatbtnPage } from './floatbtn.page';

const routes: Routes = [
  {
    path: '',
    component: FloatbtnPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FloatbtnPageRoutingModule {}
