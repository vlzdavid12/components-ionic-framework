import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FloatbtnPageRoutingModule } from './floatbtn-routing.module';

import { FloatbtnPage } from './floatbtn.page';
import {ComponentsModule} from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FloatbtnPageRoutingModule,
    ComponentsModule
  ],
  declarations: [FloatbtnPage]
})
export class FloatbtnPageModule {}
