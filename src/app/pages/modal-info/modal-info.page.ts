import { Component, OnInit, Input } from '@angular/core';
import {ModalController} from '@ionic/angular';

@Component({
  selector: 'app-modal-info',
  templateUrl: './modal-info.page.html',
  styleUrls: ['./modal-info.page.scss'],
})
export class ModalInfoPage implements OnInit {

  @Input() name: string;
  @Input() city: string;

  constructor(private modalCtr: ModalController) { }

  ngOnInit() {
  }

  exitModal(){
    this.modalCtr.dismiss();
  }

  exitModalParameter(){
    this.modalCtr.dismiss({
      name: 'David',
      city: 'Bogota'
    })
  }

}
