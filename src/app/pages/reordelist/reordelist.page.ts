import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-reordelist',
  templateUrl: './reordelist.page.html',
  styleUrls: ['./reordelist.page.scss'],
})
export class ReordelistPage implements OnInit {

  persons: string[] = ['Aquaman', 'Superman', 'Mujer Maravilla', 'Batman', 'Wolverine', 'Capitan America', 'Flahs'];

  //Create Property Boolean
  reorder =  false;

  constructor() { }

  ngOnInit() { }

  doReorder(event){
    console.log(event);

    const itemMover = this.persons.splice(event.detail.from, 1)[0];
    this.persons.splice(event.detail.to, 0, itemMover);

    event.detail.complete();

    console.log(this.persons);
  }


  onToggle(){
    this.reorder = !this.reorder;
  }

}
