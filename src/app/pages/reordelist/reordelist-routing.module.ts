import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReordelistPage } from './reordelist.page';

const routes: Routes = [
  {
    path: '',
    component: ReordelistPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReordelistPageRoutingModule {}
