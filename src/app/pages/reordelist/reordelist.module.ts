import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ReordelistPageRoutingModule } from './reordelist-routing.module';

import { ReordelistPage } from './reordelist.page';
import {ComponentsModule} from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReordelistPageRoutingModule,
    ComponentsModule
  ],
  declarations: [ReordelistPage]
})
export class ReordelistPageModule {}
