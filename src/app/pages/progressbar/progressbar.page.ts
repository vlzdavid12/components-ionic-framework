import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-progressbar',
  templateUrl: './progressbar.page.html',
  styleUrls: ['./progressbar.page.scss'],
})
export class ProgressbarPage implements OnInit {

  percentage: number = 0.05;
  constructor() { }

  ngOnInit() {
  }

  rangeChange(event){
    const {detail:{value}} = event;
    this.percentage = value / 100;
  }

}
