import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/tabs/content',
    pathMatch: 'full',
  },
  {
    path: '',
    component: TabsPage,
    children:[{
      path: 'account',
      loadChildren: ()=> import('../avatar/avatar.module').then(m=> m.AvatarPageModule)
    },
      {
        path: 'contact',
        loadChildren: ()=> import('../inputs/inputs.module').then(m=> m.InputsPageModule)
      },
      {
        path: 'settings',
        loadChildren: ()=> import('../reordelist/reordelist.module').then(m=> m.ReordelistPageModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabsPageRoutingModule {}
