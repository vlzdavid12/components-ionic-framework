import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {DataService} from '../../service/data.service';
import {Observable, Subscription} from 'rxjs';
import {IonList} from "@ionic/angular";


@Component({
  selector: 'app-list',
  templateUrl: './list.page.html',
  styleUrls: ['./list.page.scss'],
})
export class ListPage implements OnInit, OnDestroy {

  private unSubscription: Subscription;
  private dataUser: Observable<any>;

  @ViewChild(IonList) ionList: IonList;

  constructor(private dataService: DataService) { }

  ngOnInit() {
    const data: Observable<any> = this.dataService.getUsers();
    this.unSubscription = data.subscribe(item => {
      this.dataUser = item;
    });
  }

  ngOnDestroy() {
    this.unSubscription.unsubscribe();
  }


  favorite(user: any){
    console.log('favorite', user);
    this.ionList.closeSlidingItems();
  }

  share(user: any){
    console.log('share',user);
    this.ionList.closeSlidingItems();
  }

  delete(user: any){
    console.log('delete', user);
    this.ionList.closeSlidingItems();
  }


}
