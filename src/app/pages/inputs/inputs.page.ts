import {Component, OnInit, } from '@angular/core';
import {NgForm} from '@angular/forms';

interface User {
  email: string;
  password: string;
}

@Component({
  selector: 'app-inputs',
  templateUrl: './inputs.page.html',
  styleUrls: ['./inputs.page.scss'],
})
export class InputsPage implements OnInit {

  name = '';
  user: User = {
    email: '',
    password: ''
  };

  constructor() {
  }

  ngOnInit() {
  }

  onSubmit(form: NgForm) {
      console.log(form);
      console.log(this.user);
      console.log('Success fully!');
  }

}
