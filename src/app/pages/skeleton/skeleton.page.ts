import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs';
import {HeroesJson} from '../../interface/interface';
import {DataService} from '../../service/data.service';

@Component({
  selector: 'app-skeleton',
  templateUrl: './skeleton.page.html',
  styleUrls: ['./skeleton.page.scss'],
})
export class SkeletonPage implements OnInit {

  loaderSkeleton: number[] =  Array(40);


  dataHeroe: Observable<HeroesJson[]>;

  publisher: string = 'todos';

  menuData: {
    value: string;
    name: string;
  }[] = [
    {
      value: 'todos',
      name: 'Todos',
    },
    {
      value: 'DC Comics',
      name: 'DC Comics',
    }, {
      value: 'Marvel Comics',
      name: 'Marvel Comics',
    }
  ];

  constructor(private dataService: DataService) {
  }

  ngOnInit() {
    this.dataHeroe = this.dataService.getHeroes();
  }


  segmentChanged(event) {
    const {detail: {value}} = event;
    this.publisher = value;
  }

}
