import { Component, OnInit, ViewChild } from '@angular/core';
import {IonInfiniteScroll} from '@ionic/angular';

@Component({
  selector: 'app-infinitscroll',
  templateUrl: './infinitscroll.page.html',
  styleUrls: ['./infinitscroll.page.scss'],
})
export class InfinitscrollPage implements OnInit {


  data: any[] = Array(100);

  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;

  constructor() { }

  ngOnInit() {
  }

  loadData(event){
    setTimeout(()=>{
      const newArr = Array(20);

      if(this.data.length > 140){
        this.infiniteScroll.complete();
        this.infiniteScroll.disabled = true;
        return;
      }

      this.data.push(...newArr);
      //event.target.complete();
    }, 1500)

  }

}
