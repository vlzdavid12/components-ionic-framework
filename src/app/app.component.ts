import {Component, OnInit} from '@angular/core';
import {DataService} from './service/data.service';
import {Observable} from 'rxjs';
import {DataJson} from './interface/interface';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent implements OnInit {

  menuItems: Observable<DataJson[]>;

  constructor(private menuCTR: DataService) {}

  ngOnInit() {
    this.menuItems = this.menuCTR.getMenuOptions();
  }

}
